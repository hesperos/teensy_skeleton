# Teensy project skeleton

This project aims to simplify project creation for Teensy development boards. It is based on MCUXpresso SDK delivered by NXP.

# Requirements

- arm-none-eabi tool-chain
- MCUXPresso SDK

# Obtaining the SDK

Due to licensing, the SDK should be obtained individually.
The SDK should be generated using NXP's [MCUXPresso SDK Builder](https://mcuxpresso.nxp.com/) (this requires registration). It's preferable to generated SDK with FreeRTOS (in case this is needed later). Once the SDK is ready it should be unpacked into location:

    3ps/mcuxpresso/mcuxpresso

This skeleton has been tested with SDK version 2.2.0.

# Supported boards

Support is limited, at the moment, to (however, adding new platforms should be fairly trivial):

- Teensy 3.2 (mk20dx256vlh7)

# Building & deploying

In order to build the SDK, all related libraries and the project itself:

    cmake -H. -Bbld -G Ninja
    ninja -C bld
    arm-none-eabi-objdump -O ihex -R .eeprom bld/blinky.elf

In order to deploy with `teensy_cli` tools:

   sudo ~/teensy/bin/teensy_loader_cli --mcu mk20dx256 -w -v blinky.hex

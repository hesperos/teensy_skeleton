#include <fsl_clock.h>
#include <fsl_gpio.h>
#include <fsl_port.h>

const int pinNumber = 5;

void d() {
    const int n = 1000000;
    for(volatile int i = 0; i < n; ++i);
    for(volatile int i = 0; i < n; ++i);
}

int main()
{
    CLOCK_EnableClock(kCLOCK_PortC);
    PORT_SetPinMux(PORTC, pinNumber, kPORT_MuxAsGpio);

    gpio_pin_config_t gpioPin = {
        kGPIO_DigitalOutput,
        0,
    };
    GPIO_PinInit(GPIOC, pinNumber, &gpioPin);

    while (1)
    {
        GPIO_TogglePinsOutput(GPIOC, 0xff);
        d();
    }
    return 0;
}
